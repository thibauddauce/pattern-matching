<?php declare(strict_types = 1);

namespace ThibaudDauce\PatternMatching;

use stdClass;
use PHPUnit\Framework\TestCase;
use ThibaudDauce\PatternMatching\Exceptions\UnexpectedValue;
use ThibaudDauce\PatternMatching\Exceptions\PatternsMismatched;
use ThibaudDauce\PatternMatching\Exceptions\MissingPatternsDuringMatch;
use ThibaudDauce\PatternMatching\Exceptions\UnexpectedPatternsDuringMatch;

class PatternTest extends TestCase
{
    /** @test */
    function it_returns_the_correct_constant_value()
    {
        $pattern = new Pattern(['A', 'B']);

        $result = $pattern->match('A', [
            'A' => 1,
            'B' => 2,
        ]);

        $this->assertEquals(1, $result);
    }

    /** @test */
    function it_returns_the_correct_return_value_of_a_callback()
    {
        $pattern = new Pattern(['A', 'B']);

        $result = $pattern->match('A', [
            'A' => function() {
                return 1;
            },
            'B' => function() {
                $this->fail("The 'B' callback was executed even if the requested value was 'A'.");
            },
        ]);

        $this->assertEquals(1, $result);
    }

    /** @test */
    function it_calls_the_callbacks_with_specified_arguments()
    {
        $pattern = (new Pattern(['A']))
            ->with(1, ['param'], new stdClass);

        $result = $pattern->match('A', [
            'A' => function($integer, $array, $class) {
                $this->assertEquals(1, $integer);
                $this->assertEquals(['param'], $array);
                $this->assertEquals(new stdClass, $class);
            },
        ]);
    }

    /** @test */
    function it_fails_if_we_request_an_unkwown_pattern()
    {
        $pattern = new Pattern(['A', 'B']);

        try {
            $result = $pattern->match('C', [
                'A' => 1,
                'B' => 2,
            ]);

            $this->fail("Match result to {$result} even if the 'C' pattern was an unexpected value.");
        } catch (UnexpectedValue $e) {
            $this->assertEquals('C', $e->unexpectedValue);
        }
    }

    /** @test */
    function it_fails_if_a_pattern_is_forgotten_during_match()
    {
        $pattern = new Pattern(['A', 'B', 'C']);

        try {
            $result = $pattern->match('A', [
                'A' => 1,
                'B' => 2,
            ]);

            $this->fail("Match result to {$result} even if the 'C' pattern was missing.");
        } catch (MissingPatternsDuringMatch $e) {
            $this->assertEquals(['C'], array_values($e->missingPatterns));
        }
    }

    /** @test */
    function it_fails_if_there_is_an_unknown_pattern_during_match()
    {
        $pattern = new Pattern(['A', 'B']);

        try {
            $result = $pattern->match('A', [
                'A' => 1,
                'B' => 2,
                'C' => 3,
            ]);

            $this->fail("Match result to {$result} even if the 'C' pattern was unknown.");
        } catch (UnexpectedPatternsDuringMatch $e) {
            $this->assertEquals(['C'], array_values($e->unexpectedPatterns));
        }
    }

    /** @test */
    function it_fails_with_a_wider_exception_if_there_is_a_missing_pattern_and_an_unknown_pattern()
    {
        $pattern = new Pattern(['A', 'B', 'C']);

        try {
            $result = $pattern->match('A', [
                'A' => 1,
                'B' => 2,
                'D' => 4,
            ]);

            $this->fail("Match result to {$result} even if the 'C' pattern was missing and the 'D' pattern was unknown.");
        } catch (MissingPatternsDuringMatch $e) {
            $this->fail("Missing patterns raised even if the 'D' pattern was unkwown.");
        } catch (UnexpectedPatternsDuringMatch $e) {
            $this->fail("Unexpected patterns raised even if the 'C' pattern was missing.");
        } catch (PatternsMismatched $e) {
            $this->assertEquals(['C'], array_values($e->missingPatterns));
            $this->assertEquals(['D'], array_values($e->unexpectedPatterns));
        }
    }
}
