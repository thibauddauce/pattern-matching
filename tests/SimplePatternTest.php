<?php declare(strict_types = 1);

namespace ThibaudDauce\PatternMatching;

use PHPUnit\Framework\TestCase;
use ThibaudDauce\PatternMatching\Exceptions\UnexpectedValue;

class SimplePatternTest extends TestCase
{
    /** @test */
    function it_returns_the_correct_constant_value()
    {
        $result = SimplePattern::match('A', [
            'A' => 1,
            'B' => 2,
        ]);

        $this->assertEquals(1, $result);
    }

    /** @test */
    function it_fails_if_we_request_an_unkwown_pattern()
    {
        try {
            $result = SimplePattern::match('C', [
                'A' => 1,
                'B' => 2,
            ]);

            $this->fail("Match result to {$result} even if the 'C' pattern was an unexpected value.");
        } catch (UnexpectedValue $e) {
            $this->assertEquals('C', $e->unexpectedValue);
        }
    }
}
