<?php

namespace ThibaudDauce\PatternMatching;

class SimplePattern
{
    public static function match($value, $options)
    {
        return (new Pattern(array_keys($options)))
            ->match($value, $options);
    }
}
