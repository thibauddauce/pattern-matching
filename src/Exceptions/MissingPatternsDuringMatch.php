<?php declare(strict_types = 1);

namespace ThibaudDauce\PatternMatching\Exceptions;

class MissingPatternsDuringMatch extends PatternsMismatched
{
    public function __construct(array $patterns, array $actions, array $missingPatterns)
    {
        parent::__construct($patterns, $actions, $missingPatterns, []);
    }
}
