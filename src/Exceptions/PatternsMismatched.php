<?php declare(strict_types = 1);

namespace ThibaudDauce\PatternMatching\Exceptions;

class PatternsMismatched extends PatternMatchingException
{
    public function __construct(array $patterns, array $actions, array $missingPatterns, array $unexpectedPatterns)
    {
        $this->patterns = $patterns;
        $this->actions = $actions;
        $this->missingPatterns = $missingPatterns;
        $this->unexpectedPatterns = $unexpectedPatterns;

        $message = '';

        if (! empty($missingPatterns)) {
            $missingPatternsList = implode(', ', $missingPatterns);
            $message .= "$missingPatternsList were missing during the match. ";
        }

        if (! empty($unexpectedPatterns)) {
            $unexpectedPatternsList = implode(', ', $unexpectedPatterns);
            $message .= "$unexpectedPatternsList were unattended during the match. ";
        }

        $expectedPatternsList = implode(', ', $patterns);
        $receivedPatternsList = implode(', ', array_keys($actions));

        parent::__construct("{$message}Expected patterns were $expectedPatternsList and received patterns were $receivedPatternsList.");
    }
}
