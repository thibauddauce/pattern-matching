<?php declare(strict_types = 1);

namespace ThibaudDauce\PatternMatching\Exceptions;

use Exception;

class PatternMatchingException extends Exception
{
}
