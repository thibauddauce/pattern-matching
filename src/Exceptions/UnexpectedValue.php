<?php declare(strict_types = 1);

namespace ThibaudDauce\PatternMatching\Exceptions;

class UnexpectedValue extends PatternMatchingException
{
    public function __construct(array $patterns, $unexpectedValue)
    {
        $this->patterns = $patterns;
        $this->unexpectedValue = $unexpectedValue;

        $expectedPatternsList = implode(', ', $patterns);

        parent::__construct("The requested value was '$unexpectedValue' but the expected patterns were {$expectedPatternsList}.");
    }
}
