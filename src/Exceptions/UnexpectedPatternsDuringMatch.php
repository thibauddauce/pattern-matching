<?php declare(strict_types = 1);

namespace ThibaudDauce\PatternMatching\Exceptions;

class UnexpectedPatternsDuringMatch extends PatternsMismatched
{
    public function __construct(array $patterns, array $actions, array $unexpectedPatterns)
    {
        parent::__construct($patterns, $actions, [], $unexpectedPatterns);
    }
}
