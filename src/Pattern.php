<?php declare(strict_types = 1);

namespace ThibaudDauce\PatternMatching;

use ThibaudDauce\PatternMatching\Exceptions\UnexpectedValue;
use ThibaudDauce\PatternMatching\Exceptions\PatternsMismatched;
use ThibaudDauce\PatternMatching\Exceptions\MissingPatternsDuringMatch;
use ThibaudDauce\PatternMatching\Exceptions\UnexpectedPatternsDuringMatch;
use Illuminate\Validation\Rule;

class Pattern
{
    /**
     * @var array
     */
    private $patterns;

    /**
     * @var array
     */
    private $with = [];

    public function __construct(array $patterns)
    {
        $this->patterns = $patterns;
    }

    public function with(...$args)
    {
        $this->with = $args;

        return $this;
    }

    public function match($value, array $actions)
    {
        $missingPatterns = array_diff($this->patterns, array_keys($actions));
        $unexpectedPatterns = array_diff(array_keys($actions), $this->patterns);

        if (! empty($missingPatterns) and ! empty($unexpectedPatterns)) {
            throw new PatternsMismatched($this->patterns, $actions, $missingPatterns, $unexpectedPatterns);
        }

        if (! empty($missingPatterns)) {
            throw new MissingPatternsDuringMatch($this->patterns, $actions, $missingPatterns);
        }

        if (! empty($unexpectedPatterns)) {
            throw new UnexpectedPatternsDuringMatch($this->patterns, $actions, $unexpectedPatterns);
        }

        if (! isset($actions[$value])) {
            throw new UnexpectedValue($this->patterns, $value);
        }

        if (is_callable($actions[$value])) {
            return call_user_func_array($actions[$value], $this->with);
        } else {
            return $actions[$value];
        }
    }

    public function rule()
    {
        return Rule::in($this->patterns);
    }
}
