# Pattern Matching

This is a small library to pattern match on a list of enums in PHP. Please check [my blog post about it](https://thibaud.dauce.fr/posts/2017-02-13-pattern-matching-in-php.html).

## Installation

```php
composer require thibaud-dauce/pattern-matching
```

## Examples

All possibilities are explained in [the test file](tests/PatternTest.php).
